class Animal {
    constructor(name, food, location) {
        this.name = name;
        this.food = food;
        this.location = location;
    }
    makeNoise() {
        console.log(`${this.name} спить.`);
    }
    eat() {
        console.log(`${this.name} їсть ${this.food}.`);
    }
    sleep() {
        console.log(`${this.name} спить, Хррр...Хрр..`)
    }
}
class Dog extends Animal {
    constructor(name, food, location, breed) {
        super(name, food, location);
        this.breed = breed;
    }
    makeNoise() {
        console.log('Гав...гав');
    }
    eat() {
        console.log(`${this.name} їсть собачий корм та кісточки`);
    }
}

class Cat extends Animal {
    constructor(name, food, location, breed) {
        super(name, food, location);
        this.breed = breed;
    }
    makeNoise() {
        console.log('Мяуу...мяу');
    }
    eat() {
        console.log(`${this.name}їсть корм 'Гурме'`);
    }
}
class Horse extends Animal {
    constructor(name, food, location, breed) {
        super(name, food, location);
        this.breed = breed;
    }
    makeNoise() {
        console.log('Іго-го');
    }
    eat() {
        console.log(`${this.name}їсть сіно`);
    }
}
class Veterinarian {
    treatAnimal(animal) {
        console.log(`На прийомі ${animal.name}, він ${animal.food} та живе в ${animal.location}`)
    }
    main() {
        const animals = [
            new Dog("Baron", 'їсть собачий корм і кісточки', 'будці', 'лабрадор'),
            new Cat("Tom", "їсть мишей", 'дворі'),
            new Horse("Mustang", 'їсть сіно', 'стайні')
        ];
        for (let i = 0; i < animals.length; i++) {
            this.treatAnimal(animals[i]);
        }
    }
}
let vet = new Veterinarian();
vet.main ();



