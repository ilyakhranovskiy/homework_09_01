for (i = 1; i <= 5; i++) {
    for (j = i; j < 5; j++) {
        document.write("&nbsp");
    }
    for (k = 5 - i; k < 5; k++) {
        document.write("*");
    }
    document.write("<br>");
}
document.write("<br>");
// --------------------------------------------------
for (i = 1; i <= 5; i++) {
    for (j = i; j < 5; j++) {
        document.write("&nbsp");
    }
    for (k = 5 - i; k < 5; k++) {
        document.write("*");
    }
    document.write("<br>");
}
for (i = 1; i <= 5; i++) {
    for (j = 5 - i; j < 5; j++) {
        document.write("&nbsp");
    }
    for (k = i; k < 5; k++) {
        document.write("*");
    }
    document.write("<br>");
}
document.write("<br>");
// --------------------------------------------------
for (i = 0; i < 5; i++) {
    for (j = 0; j < 5; j++) {
        document.write("&nbsp*");
    }
    document.write("<br>");
}
document.write("<br>");
// -------------------------------------------------
let fullstr = ("&nbsp*").repeat(5);
for (i = 0; i < 5; i++) {
    let str = (i === 0 || i === 4) ? fullstr : "&nbsp*" + ("&nbsp").repeat(10) + "*";
    document.write(str + "<br>");
}

