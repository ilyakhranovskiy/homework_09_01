class Car {
    constructor(brand, carClass, engine, driver) {
        this.brand = brand;
        this.carClass = carClass;
        this.engine = engine;
        this.driver = driver;
    }
    start() {
        console.log("Поїхали");
    }
    stop() {
        console.log("Зупиняємось");
    }
    turnLeft() {
        console.log("Поворот ліворуч");
    }
    turnRight() {
        console.log("Поворот праворуч");
    }
    toString() {
        return (`
        Марка: ${this.brand}, Клас:${this.carClass}, 
        Водій:${this.driver.toString()}, 
        Потужність двигуна ${this.engine.power} кінських сил, виробник ${this.engine.company}`);
    }
}
class Engine {
    constructor(power, company) {
        this.power = power;
        this.company = company;
    }
}
class Driver {
    constructor(experience) {
        this.experience = experience;
    }
}
class Person extends Driver {
    #age;
    constructor(age, fullName, experience) {
        super(experience);
        this.#age = age;
        this.fullName = fullName;
    }
    toString() {
        return `Ім'я:${this.fullName}, Вік:${this.#age}, Стаж вождіння:${this.experience}років`;
    }
}
class Lorry extends Car {
    constructor(brand, carClass, engine, driver, cargoCapacity) {
        super(brand, carClass, engine, driver);
        this.cargoCapacity = cargoCapacity;
    }
}

class SportCar extends Car {
    constructor(brand, carClass, engine, driver, maxSpeed) {
        super(brand, carClass, engine, driver);
        this.maxSpeed = maxSpeed;
    }
}
const engine = new Engine(300, 'Audi');
const driver = new Driver(10);
const person = new Person(44, 'Петренко Андрій Ігорович', driver.experience);
const car = new Car('Audi', 'sportback', engine, person);
const lorry = new Lorry('Volvo', 'truck', engine, person, 1000);
const sportCar = new SportCar('Ferrari', 'supercar', engine, person, 300);
console.log(car.toString());
console.log(lorry.toString());
console.log(sportCar.toString());