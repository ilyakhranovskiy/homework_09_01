// --------------1-----------------
let arr1 = ['a', 'b', 'c'];
let arr2 = [1, 2, 3];
let newArr = arr1.concat(arr2);
document.write(newArr + "<br>");
// --------------2-----------------
arr1.push(1, 2, 3);
document.write(arr1 + "<br>");
// --------------3-----------------
let reverseArr2 = arr2.reverse();
document.write(reverseArr2 + "<br>");
// --------------4-----------------
let arr3 = [1, 2, 3];
arr3.push(4, 5, 6);
document.write(arr3 + "<br>");
// --------------5-----------------
let arr4 = [1, 2, 3];
arr4.unshift(4, 5, 6);
document.write(arr4 + "<br>");
// --------------6-----------------
let arr5 = ['js', 'css', 'jq'];
document.write(arr5[0] + "<br>");
// --------------7-----------------
let arrFirst = [1, 2, 3, 4, 5];
let arrNew = arrFirst.slice(0, 3);
document.write(arrNew + "<br>");
// --------------8-----------------
let arr6 = [1, 2, 3, 4, 5];
arr6.splice(1, 2);
document.write(arr6 + "<br>");
// --------------9-----------------
let arr7 = [1, 2, 3, 4, 5];
arr7.splice(2, 0, 10);
document.write(arr7 + "<br>");
// -------------10-----------------
let arrUnsorted = [3, 4, 1, 2, 7];
let arrSorted = arrUnsorted.sort();
document.write(arrSorted + "<br>");
// --------------11----------------
let arr8 = ['Привіт,', 'світ', '!'];
document.write(arr8.join(' ') + "<br>");
// --------------12-----------------
let arr9 = ['Привіт,', 'світ', '!'];
arr9.splice(0, 1, 'Бувай,');
document.write(arr9.join(' ') + "<br>");
// --------------13-----------------
let arr10 = [1, 2, 3, 4, 5];
let arr11 = new Array(1, 2, 3, 4, 5);
document.write(arr10 + "<br>" + arr11 + "<br>");
// --------------14-----------------
let arr = [
    ['блакитний', 'червоний', 'зелений'],
    ['blue', 'red', 'green'],
];
document.write(arr[0][0], arr[1][0] + "<br>");
// -------------15------------------
let arr12 = ['a', 'b', 'c', 'd'];
document.write(arr12[0] + "+" + arr12[1] + "," + arr12[2] + "+" + arr12[3] + "<br>");
// -------------16------------------
let a = +prompt("Input the number");
let b = [];
for (i = 0; i <= a; i++){
    b.push(i);
}
document.write(b+"<br>");
// -------------17-----------------
let c=[];
let d=[];
for(i in b){
    if(i%2!==0){
        c.push(i);
    }else{
        d.push(i)
    }
}
document.write(`<h3>${c}</h3>`+"<br>"+`<span style=background-color:red;>${d}</span>`+"<br>");
// --------------18-----------------
let vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морква'];
// let str=vegetables.split(' ');
let str1=vegetables.join(',');
document.write(str1);
// 


