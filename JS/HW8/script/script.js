window.onload = () => {
    //  #1

    const changeBtn = document.getElementById("changeBtn");
    const btn1 = document.getElementById("btn1");
    const btn2 = document.getElementById("btn2");

    changeBtn.onclick = () => {
        const temp = btn1.value;
        btn1.value = btn2.value;
        btn2.value = temp;
    }

    //  #2

    const [...divs] = document.getElementsByClassName("testDiv");

    divs.forEach(element => {
        element.style.backgroundColor = "lightgreen";
    });

    // #3

    const createBtn = document.getElementById("createBtn");
    const container = document.querySelector(".container");

    createBtn.onclick = () => {

        const text = document.getElementById("textarea").value
        const div = document.createElement("div");

        div.append(text);
        container.append(div);

        document.getElementById("textarea").value = '';
    }

    // #4

    const changeImgBtn = document.getElementById("changeImgBtn");
    changeImgBtn.onclick = () => {

        const img1 = document.querySelector("#img1");
        const img2 = document.querySelector("#img2");
        const img3 = document.querySelector("#img3");

        img2.style.display = "inline-block";
        img1.replaceWith(img2);

        changeImgBtn.onclick = () => {
            img3.style.display = "inline-block";
            img2.replaceWith(img3);
        }
    }

    // #5

    for (let i = 0; i < 10; i++) {
        const tenP = document.createElement("p");
        tenP.append("Paragraph");
        document.querySelector("#tenP").append(tenP)

        tenP.onclick = () => tenP.remove();
    }

    // #6
    // --------
    const buttonPickFirst = document.getElementById("button1")

    buttonPickFirst.onclick = () => {

        let node = list.querySelector("[style='color:green;']");
        if (node != null) {
            const list = document.getElementById("list");
            node.removeAttribute("style");
        }

        const firstChild = list.firstElementChild;
        if (firstChild != null) {
            firstChild.setAttribute("style", "color:green;");
        }
    }

    // --------

    const buttonPickLast = document.getElementById("button2")

    buttonPickLast.onclick = () => {

        let node = list.querySelector("[style='color:green;']")
        if (node != null) {
            const list = document.getElementById("list");
            node.removeAttribute("style");
        }

        const lastChild = list.lastElementChild;
        if (lastChild != null) {
            lastChild.setAttribute("style", "color:green;");

        }
    }

    // ---------

    const selectNextNode = document.getElementById("button3");
    selectNextNode.onclick = () => {
        const list = document.getElementById("list");
        let node = list.querySelector("[style='color:green;']");

        if (node == null) {
            list.firstElementChild.setAttribute("style", "color:green;");
            return
        }
        if (node != null) {
            node.removeAttribute("style");
            if (node.nextElementSibling != null) {
                node = node.nextElementSibling;
                node.setAttribute("style", "color:green;");
            }
        }

    }

    // ----------

    const selectPrevNode = document.getElementById("button4");
    selectPrevNode.onclick = () => {
        const list = document.getElementById("list");
        let node = list.querySelector("[style='color:green;']");

        if (node == null) {
            list.lastElementChild.setAttribute("style", "color:green;");
            return
        }

        if (node != null) {
            node.removeAttribute("style");
            if (node.previousElementSibling != null) {
                node = node.previousElementSibling;
                node.setAttribute("style", "color:green;");
            }
        }
    }

    // ----------

    const addElement = document.getElementById("button5");
    addElement.onclick = () => {
        const list = document.getElementById("list")
        createLi = document.createElement("li");

        const index = list.getElementsByTagName("li").length;

        createLi.textContent = `List Item ${index + 1}`;
        list.append(createLi);
    }

    // ----------

    const deleteElement = document.getElementById("button6")
    deleteElement.onclick = () => {
        const list = document.getElementById("list");
        last = list.lastElementChild;
        if (last != null) {
            list.removeChild(last);
        }
    }

    // ----------

    const addElementAtStart = document.getElementById("button7");
    addElementAtStart.onclick = () => {
        const list = document.getElementById("list");
        const createLiAtStart = document.createElement("li");

        const index = list.getElementsByTagName("li").length;

        createLiAtStart.textContent = `List Item At Start`;
        list.insertBefore(createLiAtStart, list.firstElementChild);
    }

}
