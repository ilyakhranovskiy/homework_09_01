
const commentsPerPage = 100;
let currentPage = 1;


async function fetchComments() {
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/comments');
        const comments = await response.json();
        return comments;
    } catch (error) {
        console.log("error");
        return [];
    }
}
function displayComments(comments, start, end) {
    const commentsList = document.getElementById('comments-list');
    commentsList.innerHTML = '';

    for (let i = start; i < end; i++) {
        const comment = comments[i]
        const listItem = document.createElement('li');
        listItem.innerHTML = `
        <p><strong>ID:</strong>${comment.id}</p>
        <p><strong>Name:</strong>${comment.name}</p>
        <p><strong>Email:</strong>${comment.email}</p>
        <p><strong>Comment:</strong>${comment.body}</p>`

        commentsList.appendChild(listItem)
    }
}

function displayPagination(comments) {
    const pagination = document.getElementById('pagination');
    pagination.innerHTML = '';

    const totalPages = Math.ceil(comments.length / commentsPerPage);

    for (let i = 1; i <= totalPages; i++) {
        const link = document.createElement('span');
        link.textContent = i;
        link.classList.add('pagination-link');
        if (i === currentPage) {
            link.classList.add('active')
        }
        link.addEventListener('click', () => {
            currentPage = i;
            const start = (currentPage - 1) * commentsPerPage;
            const end = start + commentsPerPage;
            displayComments(comments, start, end);
            displayPagination(comments);
        })
        pagination.appendChild(link);
    }
}

async function init() {
    const start = (currentPage - 1) * commentsPerPage,
        end = start + commentsPerPage,
        comments = await fetchComments();
    displayComments(comments, start, end);
    displayPagination(comments);
}

init();

