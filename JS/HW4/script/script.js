// --------------------------------------------------#1-----------------------------
/*let num1 = +prompt("Input first number");
while (!num1) {
    num1 = +prompt("Input first number");
}
let num2 = +prompt("Input second number");
while (!num2) {
    num2 = +prompt("Input second number");
}
let operator = prompt("Input  operation symbol:'+', '-', '*', '/'");
while (operator != '+' && operator != '-' && operator != '*' && operator != '/') {
    operator = prompt("Input  operation symbol:'+', '-', '*', '/'");
}

function calcNum(a, b, c) {

    switch (c) {
        case "+": return console.log(a + b);
        case "-": return console.log(a - b);
        case "/": return console.log(a / b);
        case "*": return console.log(a * b);
    }
}
calcNum(num1, num2, operator);*/
// --------------------------------------------------#2----------------------------
arr1 = [1, 2, 3, 4];

function map(fn, arr) {
    const res = [];
    for (let i = 0; i < arr.length; i++) {
        res.push(fn(arr[i]));
    }
    return console.log(res);
}

map(n => n * n, arr1);


function factorial(n) {
    if (n === 0 || n === 1) {
        return 1;
    } else {
        return n * factorial(n - 1);
    }
}

map(factorial, arr1);
// ------------------------------------------------#3-----------------------------
function checkAge(age) {
    age > 18 ? true : confirm('Батьки дозволили?');
}
checkAge(19);
// ------------------------------------------------#4-----------------------------
let str = "var_text_hello";
let parts = str.split('_');
console.log(parts);
for (i = 0; i < parts.length; i++) {
    parts[i] = parts[i][0].toUpperCase() + parts[i].slice(1);
}
console.log(parts.join(''));
//-------------------------------------------------#5-----------------------------
function ggg(func1, func2) {
    return func1() + func2();
}
const sum = ggg(() => 3, () => 4);
console.log(sum);
// ------------------------------------------------#6-----------------------------
const cryptoWallet = {
    userName: "Petro",
    bitcoin: {
        currencyName: "Bitcoin",
        logo: `<img src="./img/BitcoinSign.svg.png" alt="logo">`,
        coins: 2.5,
        todayRate: 1260123.45
    },
    ethereum: {
        currencyName: "Ethereum",
        logo: `<img src="./img/Ethereum-Logo.wine.png" alt="logo">`,
        coins: 10,
        todayRate: 54689.12
    },
    stellar: {
        currencyName: "Stellar",
        logo: `<img src="./img/stellar-xlm-logo.png" alt="logo">`,
        coins: 500,
        todayRate: 231.67
    },
    getCurrencyBalance: function (currencyName) {
        const currency = this[currencyName.toLowerCase()];
        if (!currency) {
            return (`Sorry, ${this.userName}, ${currencyName} is not supported in your wallet`);
        }
        const balance = currency.coins;
        const balanceInUAH = balance * currency.todayRate;
        return (`Доброго дня, ${this.userName}! На вашому балансі ${currency.currencyName}  ${currency.logo} &nbsp залишилося ${balance} монет, якщо ви сьогодні продасте їх те, отримаєте ${balanceInUAH} грн.`);
    }
};
document.getElementById("crypto").innerHTML = (cryptoWallet.getCurrencyBalance("stellar"));
// ------------------------------------------------#7---------------------------------
class Worker {
    constructor(name, surname, rate, days) {
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days;
    }

    getSalary() {
        return this.rate * this.days;
    }
}


const worker = new Worker('Іван', 'Іванов', 200, 20);
console.log(`${worker.name} ${worker.surname} отримує зарплату ${worker.getSalary()} грн.`);
//   ------------------------------------------#8-------------------------------------------------
class MyString {
    constructor(str) {
        this.str = str;
    }

    reverse() {
        return this.str.split('').reverse().join('');
    }

    ucFirst() {
        return this.str.charAt(0).toUpperCase() + this.str.slice(1);
    }

    ucWords() {
        return this.str.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
    }
}

const myString = new MyString('hello world');
console.log(myString.reverse());
console.log(myString.ucFirst());
console.log(myString.ucWords());
// -----------------------------------------------------------------------------
