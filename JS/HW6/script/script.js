/*
- Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
- При виклику функція повинна запитати у імені, що викликає, і прізвище.
- Використовуючи дані, введені користувачем, створити об'єкт newUser з властивостями firstName та lastName.
- Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені користувача, з'єднану з прізвищем користувача, все в нижньому регістрі (наприклад, `Ivan Kravchenko → ikravchenko`).
- Створити користувача за допомогою функції createNewUser(). Викликати у користувача функцію getLogin(). Вивести у консоль результат виконання функції.

- Візьміть виконане вище завдання (створена вами функція `createNewUser()`) та доповніть її наступним функціоналом:
   1. При виклику функція повинна запитати в дату народження (текст у форматі dd.mm.yyyy`) і зберегти її в полі `birthday.
   2. Створити метод getAge() який повертатиме скільки користувачеві років.
   3. Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, поєднану з прізвищем (у нижньому регістрі) та роком народження. (Наприклад, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`).
- Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.
*/

/*
class CreateNewUser {
    constructor(name, surname, birthday) {
        this.firstName = name
        this.lastName = surname
        this.birthday = birthday 
 
    }
    getLogin() {
       return (this.firstName[0] + this.lastName).toLowerCase()
    }
    getAge() {

        const now = new Date().getFullYear();
        const birthYear = this.birthday.split('.')[2];
        let age = now - birthYear;
        console.log(`Користувачеві ${age} років`);
    }
    getPassword() {
       return  this.firstName[0].toUpperCase()+this.lastName.toLowerCase()+this.birthday.split('.')[2];
    }

}
const newUser = new CreateNewUser(prompt('Введіть імя'), prompt('Введіть прізвище'), prompt('Введіть дату народження(dd.mm.yyyy)'));

*/
// ---------------------------------------------------------------------------------------------------------------------------------------------------
/*2. 
Використовуючи класи створити з об'єктів 
https://fakestoreapi.com/products об'єкти 
{
            name : "string"
            image : "string"
            productPrice : "number"
            productDescription : "stering"
        }
На виході створити карточки товарів з використанням бібліотеки bootstrap
*/
for (let i = 0; i < products.length; i++) {
    document.getElementById("block").innerHTML += (
        ` <div class="card" style="width: 18rem;">
        <img src="${products[i].image}" class="card-img-top my-img" alt="${products[i].title}">
        <div class="card-body my-body d-flex flex-column">
            <h5 class="card-title">${products[i].title}</h5>
            <p class="card-text">${products[i].description}</p>
            <p class="card-text">Price:${products[i].price}</p>
            <a href="#" class="btn btn-primary mt-auto">Buy now</a>
        </div>
        </div>`
    );
}

/*3.
Написати функцію filterBy(), яка прийматиме 2 аргументи.Перший аргумент - масив,
    який міститиме будь - які дані, другий аргумент - тип даних.
Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент,
    за винятком тих, тип яких був переданий другим аргументом. 
Тобто якщо передати масив['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив[23, null].
*/
function filterBy(arr, type) {
    if (type === 'null' || type === null) {
        return arr.filter(item => item !== null);
    } else {
        return arr.filter(item => typeof item !== type)
    }
}

let filtered = filterBy([null, 'hello', 11, 'world', null, 23], 'number');
console.log(filtered);

