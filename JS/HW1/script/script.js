var name_ = prompt("Whats your name?")
var surname_ = prompt("Whats your surname?")
var age_ = prompt("How old are you?")
var country = prompt("Where are you from?")
document.write("<header>" + name_ + "<br>" + surname_ + "<br>" + age_ + " <br>" + country + " </header>");
document.write("<nav></nav>");
document.write("<section></section>")
document.write("<footer></footer>");
console.log("Name:" + name_ + surname_ + "\n" + "Age:" + age_ + "\n" + "Country:" + country);

var x = 6, y = 14, z = 4;
num = x += y - x++ * z;
document.write(num + "<br>");
// 1.x++ постфікс. інкремент, x=6
// 2.множення  6*4
// 3.віднімання 14-24
// 4.розумне присвоєння(додавання) іксу числа, справа від знака, 6-10
// результат -4 

var x = 6, y = 14, z = 4;
num = z = --x - y * 5;
document.write(num + "<br>");
// 1.--x префікс. декремент, x=5
// 2.множення 14*5
// 3.віднімання 5-70
// 4.присвоєння z=-65

var x = 6, y = 14, z = 4;
num = y /= x + 5 % z;
document.write(num + "<br>");
// 1.залишок від ділення 5/4=1
// 2.додавання 6+1
// 3.розумне присвоєння(ділення) ігрику, 14/7
// 4.результат 2

var x = 6, y = 14, z = 4;
num = z - x++ + y * 5;
document.write(num + "<br>");
//1.x++ постфікс. інкремент x=6
//2.множення 14*5
//3.у випадку однакової пріоритетності, користуємось асоціативністю(в даному випадку зліва направо), 
// тому спершу віднімання 4-6=-2
//4.додавання -2+70
//5.результат 68

var x = 6, y = 14, z = 4;
num = x = y - x++ * z;
document.write(num);
//1.x++ x=6
//2.множення 6*4
//3.віднімання 14-24
//4.результат -10 