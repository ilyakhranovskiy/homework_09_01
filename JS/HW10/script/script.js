let firstNumber = "";
let secondNumber = "";
let operator = "";
let memory = "";
let result = 0;



const display = document.querySelector(".display input[type='text']")
const keys = document.querySelectorAll(".keys input");
const memoryIndicator = document.querySelector(".mrc");


keys.forEach(key => {
    key.addEventListener("click", (e) => {
        let value = e.target.value;
        if (isNumber(value)) {                   //1 
            if (operator === "") { //обробка першого числа
                display.value += value;
                firstNumber += value;
                console.log("first:" + firstNumber);
            } else if (operator !== "") { //обробка другого числа
                display.value += value;
                secondNumber += value;
                document.querySelector("#eq").removeAttribute("disabled");
                console.log("second:" + secondNumber);
            }


        } else if (isOperator(value)) {          //2 
            if (operator === "") {
                if (result && display.value === result.toString()) { 
                    //умова на випадок, якщо результат, буде використаний в ролі першого числа,
                    //  порівняння з display.value на випадок,
                    //  якщо було натиснуто "mrc" і воно замінило result на дисплеї 
                    firstNumber = result.toString();
                    console.log("Result as first number:" + firstNumber);
                    result = "";
                } 
                display.value += value
                operator = value;
                console.log("opeartor:" + operator);
            }
        }
        else if (value === "C") {               //3      
            clearDisplay();
            equalDisabled();
            resetCalc();
        }

        else if (value === "=") {               //4
            calculate();
            resetCalc();
        }

        else if (value === "m+") {              //5
            memoryIndicator.style.setProperty("display", "block");
            memory = parseFloat(display.value);
            console.log("memory:" + memory);
        }

        else if (value === "m-") {              //6
            memory = memory - parseFloat(display.value);
            console.log("memory:" + memory);
        }

        if (value === "mrc") {             //7
            memoryIndicator.style.removeProperty("display", "block");
            if (firstNumber === "") {           //код, для використання числа з пам'яті, як операнда
                firstNumber = memory;
                display.value = firstNumber;

            } else if (firstNumber !== "" && operator === "") {
                firstNumber = memory;
                display.value = memory;

            } else if (firstNumber !== "" && operator !== "") {
                secondNumber = memory;
                document.querySelector("#eq").removeAttribute("disabled");
                display.value += secondNumber;
            }
            memory = "";
            console.log("memory is clear");
        }
    })
})
function isNumber(value) {
    return /[0-9.]/.test(value);
}

function isOperator(value) {
    const operators = ["+", "-", "*", "/"];
    return operators.includes(value);
}

function clearDisplay() {
    display.value = "";
    result = 0;
}

function equalDisabled() {
    document.querySelector("#eq").setAttribute("disabled", true);
}

function resetCalc() {
    operator = "";
    firstNumber = "";
    secondNumber = "";
}

function calculate() {
    num1 = parseFloat(firstNumber);
    num2 = parseFloat(secondNumber);
    switch (operator) {
        case "+": result = num1 + num2;
            break;
        case "-":
            result = num1 - num2;
            break;
        case "*":
            result = num1 * num2;
            break;
        case "/":
            result = num1 / num2;
            break;
    }
    if (!Number.isInteger(result)) {
        display.value = result.toFixed(3);//якщо результат не ціле число, округляємо
    } else {
        display.value = result.toString();
    }
    console.log("result:" + result);
}


