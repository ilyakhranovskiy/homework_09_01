const pizza = {
    userName: '',
    phone: '',
    email: '',
    size: 85,
    toppings: [],
    sauce: []
}

const price = {
    size: {
        small: 50,
        mid: 80,
        big: 85
    },
    topping: [
        { price: 20, name: "moc1", quantity:1 },
        { price: 45, name: "moc2", quantity:1 },
        { price: 12, name: "moc3", quantity:1 },
        { price: 93, name: "telya", quantity:1 },
        { price: 78, name: "vetch1", quantity:1 },
        { price: 34, name: "vetch2", quantity:1 },
    ],
    sauce: [
        { price: 60, name: "sauceClassic", quantity: 1 },
        { price: 70, name: "sauceBBQ", quantity: 1 },
        { price: 50, name: "sauceRikotta", quantity: 1 },
    ]
}
// вибір розміру піци
window.addEventListener("DOMContentLoaded", () => {
    document.querySelector("form#pizza")
        .addEventListener("click", (e) => {
            //console.log(e.target.id);
            switch (e.target.id) {
                case "small": pizza.size = price.size.small;
                    break;
                case "mid": pizza.size = price.size.mid;
                    break;
                case "big": pizza.size = price.size.big;
                    break;
            }
            show(pizza)
        })
    show(pizza)
    document.querySelector("#banner")
        .addEventListener("mousemove", (e) => {
            randomPositionBanner(e.target, e.clientX, e.clientY)
        })

    document.querySelector("#banner button")
        .addEventListener("click", () => {
            alert("Ваш промокод : XXXXXX")
        })
})

function randomPositionBanner(banner) {
    const coords = {
        X: Math.floor(Math.random() * document.body.clientWidth),
        Y: Math.floor(Math.random() * document.body.clientHeight)
    }

    const width = (parseInt(getComputedStyle(document.querySelector("#banner"))["width"]) + 100)

    if (coords.X + width > document.body.clientWidth) {
        return
    }

    if (coords.Y + 100 > document.body.clientWidth) {
        return
    }
    // console.log(coords)

    banner.style.left = coords.X + "px"
    banner.style.top = coords.Y + "px"
}

//валідація 
window.addEventListener('DOMContentLoaded', () => {
    const userName = document.querySelector("[name='name']");
    const userPhone = document.querySelector("[name='phone']");
    const userEmail = document.querySelector("[name='email']");
    const validate = (value, pattern) => pattern.test(value);

    userName.addEventListener('input', () => {
        if (validate(userName.value, /^[а-яіїєґ]{2,}$/i)) {
            userName.classList.add('success');
            userName.classList.remove('error');
            pizza.userName = userName.value;
        }
        else {
            userName.classList.remove('success');
            userName.classList.add('error');
        }
    })


    userPhone.addEventListener('input', () => {
        if (validate(userPhone.value, /^\+380[0-9]{9}$/)) {
            userPhone.classList.add('success');
            userPhone.classList.remove('error');
            pizza.phone = userPhone.value;
        }
        else {
            userPhone.classList.remove('success');
            userPhone.classList.add('error');
        }
    })
    userEmail.addEventListener('change', () => {
        if (validate(userEmail.value, /^[a-z0-9._]{3,40}@[a-z0-9-]{1,777}\.[.a-z]{2,10}$/i)) {
            userEmail.classList.add('success');
            userEmail.classList.remove('error');
            pizza.email = userEmail.value;
        }
        else {
            userEmail.classList.remove('success');
            userEmail.classList.add('error');
        }
    })

});


//Відображення складу та ціни
function show(pizza) {
    const priceElement = document.querySelector("#price");
    let totalPrice = pizza.size;

    const sauceS = document.querySelector("#sauce");
    pizza.sauce.forEach(sauce => {
        const sauceElement = document.createElement('span');
        const existingSauce = sauceS.querySelector(`span[data-sauce="${sauce.name}"]`);
        if (existingSauce) {
            existingSauce.innerText = `${sauce.name} x${sauce.quantity}`;
            totalPrice += sauce.price * sauce.quantity;
        } else {
            sauceElement.innerText = `${sauce.name} x${sauce.quantity}`;
            sauceElement.setAttribute("data-sauce", sauce.name);
            sauceS.appendChild(sauceElement);
            totalPrice += sauce.price;
        }
    });

    const topingsS = document.querySelector("#topping");
    pizza.toppings.forEach(topping => {
        const toppingElement = document.createElement('span');
        const existingTopping = topingsS.querySelector(`span[data-sauce="${topping.name}"]`);
        if (existingTopping) {
            existingTopping.innerText = `${topping.name} x${topping.quantity}`;
            totalPrice += topping.price * topping.quantity;
        } else {
            toppingElement.innerText = `${topping.name} x${topping.quantity}`;
            toppingElement.setAttribute("data-sauce", topping.name);
            topingsS.appendChild(toppingElement);
            totalPrice += topping.price;
        }
    });

    priceElement.innerText = totalPrice;
}

//Перетягування.
window.addEventListener("DOMContentLoaded", () => {
    const ingridients = document.querySelector(".ingridients"),
        table = document.querySelector(".table");

    ingridients.addEventListener("dragstart", (e) => {
        //e.target.classList.add("transfer")
        e.dataTransfer.setData("text", e.target.id);
    });

    table.addEventListener("dragenter", () => {
        table.classList.add("transfer")
    })

    table.addEventListener("dragleave", () => {
        table.classList.remove("transfer")
    })

    table.addEventListener("dragover", (e) => {
        e.preventDefault();
        e.stopPropagation();
    })

    table.addEventListener("drop", (e) => {
        e.preventDefault();

        const id = e.dataTransfer.getData("text")

        const img = document.createElement("img");
        img.src = document.getElementById(id).src;
        table.append(img)
        table.classList.remove("transfer");

        // Звіряємо отриманне id з назами соусів в об'єкті price, якщо є співпадіння, запускаємо відповідну функцію
        if (price.sauce.find(el => el.name === id)) {
            getSauce(id);
        } else {
            getTopping(id);
        }

        show(pizza);
    })
})

function getSauce(id) {
    const sauce = price.sauce.find(el => el.name === id);
    const selectedSauce = sauce.name;
    console.log(selectedSauce);

    const selectedSauceObject = price.sauce.find(obj => obj.name === selectedSauce);
    console.log(selectedSauceObject);

    if (selectedSauce) {
        const existingSauce = pizza.sauce.find(el => el.name === selectedSauce);
        if (existingSauce) {
            existingSauce.quantity++;
            console.log(existingSauce.quantity);
        }
        else {
            pizza.sauce.push(selectedSauceObject);
        }
    }
    console.log(pizza.sauce);
}

function getTopping(id) {
    const topping = price.topping.find(el => el.name === id);
    const selectedTopping = topping.name;
    console.log(selectedTopping);

    const selectedToppingObject = price.topping.find(obj => obj.name === selectedTopping);
    console.log(selectedToppingObject);

    if (selectedTopping) {
        const existingTopping = pizza.toppings.find(el => el.name === selectedTopping);
        if (existingTopping) {
            existingTopping.quantity++;
            console.log(existingTopping.quantity);
        }
        else {
            pizza.toppings.push(selectedToppingObject);
        }
    }
    console.log(pizza.toppings);
}

