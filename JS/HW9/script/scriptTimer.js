const get = id => document.getElementById(id);


let second = 0,
    minute = 0,
    hour = 0,
    interval,
    flag = false;


const counter = () => {
    const secondsDisplay = get("secondsDisplay");
    const minuteDisplay = get("minuteDisplay");
    const hourDisplay = get("hourDisplay");

    second++;
    if (second < 10) {
        secondsDisplay.textContent = "0" + second;
    }
    if (second > 9) {
        secondsDisplay.textContent = second;

    }
    if (second > 60) {
        minute++
        minuteDisplay.textContent = "0" + minute;
        second = 0;
        secondsDisplay.textContent = "0" + second;
    }
    if (minute > 9) {
        minuteDisplay.textContent = minute;
    }
    if (minute > 60) {
        hour++
        hourDisplay.textContent = "0" + hour;
        minute = 0;
        minuteDisplay.textContent = "0" + minute;
    }
    if (hour > 9) {
        hourDisplay.textContent = hour;
    }


}

const startBtn = get("startBtn");
startBtn.onclick = () => {
    clearInterval(interval);
    if (!flag) {
        interval = setInterval(counter, 1000);
        flag = true;
    } else {
        flag = false;
        interval = setInterval(counter, 1000);
    }

    const containerWatch = document.querySelector(".container-stopwatch");
    containerWatch.classList.remove("black","red","silver");
    containerWatch.classList.add("green");
}


const stopBtn = get("stopBtn");
stopBtn.onclick = () => {
    clearInterval(interval);
    flag = false;

    const containerWatch = document.querySelector(".container-stopwatch");
    containerWatch.classList.remove("black","green","silver");
    containerWatch.classList.add("red");
}

const resetBtn = get("resetBtn");
resetBtn.onclick = () => {
    clearInterval(interval);
    flag = false;
    second = 0;
    minute = 0;
    hour = 0;
    secondsDisplay.textContent = "00";
    minuteDisplay.textContent = "00";
    hourDisplay.textContent = "00";

    const containerWatch = document.querySelector(".container-stopwatch");
    containerWatch.classList.remove("black","red","green");
    containerWatch.classList.add("silver");
}

