const body = document.querySelector("body");
const div = document.createElement("div");
const input = document.createElement("input");
const saveBtn = document.createElement("button");

saveBtn.textContent = "Save";

body.append(div);
div.append(input);
div.append(saveBtn);

const pattern = /0\d{2}-\d{3}-\d{2}-\d{2}/;

let alertDisplay = false;

saveBtn.onclick = () => {
    const number = input.value;

    if (!pattern.test(number)) {
        if (!alertDisplay) {
            const alert = document.createElement("span");
            div.insertBefore(alert, input);
            alert.textContent = "Номер невірний, спробуйте ще раз";
            alert.style.color = "red";
            alert.style.display = "block";
            alertDisplay = true;
        }
    };

    if (pattern.test(number)) {
        if (alertDisplay) {
            div.firstElementChild.remove();
            input.style.animationName = "change";
            input.style.animationDuration = "3s";

            setTimeout(() => {
                document.location.assign("http://surl.li/gzcwb")
            }, 3000);
        } else {
            input.style.animationName = "change";
            input.style.animationDuration = "3s";

            setTimeout(() => {
                document.location.assign("http://surl.li/gzcwb")
            }, 3000);
        }
    }
}
